package pl.angrynerds.angrynerdsdemo.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.List;

import pl.angrynerds.angrynerdsdemo.R;
import pl.angrynerds.angrynerdsdemo.adapters.RepositoryAdapter;
import pl.angrynerds.angrynerdsdemo.apiconnection.RestClient;
import pl.angrynerds.angrynerdsdemo.models.Repository;
import pl.angrynerds.angrynerdsdemo.databinding.ActivityMainBinding;
import pl.angrynerds.angrynerdsdemo.viewmodels.MainActivityViewModel;
import pl.angrynerds.angrynerdsdemo.viewmodels.interfaces.IMainActivityAccess;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements IMainActivityAccess {

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    private MainActivityViewModel viewModel;
    private ActivityMainBinding binding;

    private RepositoryAdapter repositoryAdapter;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        viewModel = new MainActivityViewModel(this);
        binding.setViewModel(viewModel);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        setRecyclerViewParams();

        repositoryAdapter = new RepositoryAdapter(viewModel, this);
        binding.setAdapter(repositoryAdapter);

        tryToLoadRepositoriesFromTheServer();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        RestClient.clear();
    }

    private void setRecyclerViewParams() {
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mRecyclerView.setHasFixedSize(true);
    }

    @Override
    public void openRepository(Repository repository) {
        Intent intent = RepositoryDetailsActivity.createOpenRepositoryDetailsIntent(this, repository);
        startActivity(intent);
    }

    private void tryToLoadRepositoriesFromTheServer() {

        createAndShowProgressDialog();

        Call<List<Repository>> call = RestClient.getClient().getGoogleRepositories();

        call.enqueue(new Callback<List<Repository>>() {
            @Override
            public void onResponse(Call<List<Repository>> call, Response<List<Repository>> response) {
                repositoryAdapter.setRepositories(response.body());
                repositoryAdapter.notifyDataSetChanged();

                dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<List<Repository>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), getString(R.string.collect_data_error), Toast.LENGTH_LONG).show();

                dismissProgressDialog();
            }
        });
    }

    private void createAndShowProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading_repositories));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void dismissProgressDialog() {
        progressDialog.dismiss();
    }
}
