package pl.angrynerds.angrynerdsdemo.activities;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import pl.angrynerds.angrynerdsdemo.R;
import pl.angrynerds.angrynerdsdemo.models.Repository;
import pl.angrynerds.angrynerdsdemo.databinding.ActivityRepositoryDetailsBinding;
import pl.angrynerds.angrynerdsdemo.viewmodels.RepositoryDetailsActivityViewModel;
import pl.angrynerds.angrynerdsdemo.viewmodels.interfaces.IRepositoryDetailsActivityAccess;

public class RepositoryDetailsActivity extends AppCompatActivity implements IRepositoryDetailsActivityAccess{

    private static final String INTENT_REPOSITORY = "INTENT_REPOSITORY";

    private RepositoryDetailsActivityViewModel viewModel;
    private ActivityRepositoryDetailsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Repository repository = (Repository) getIntent().getSerializableExtra(INTENT_REPOSITORY);
        if(repository == null)
            throw new IllegalArgumentException("Repository object is null!");

        binding = DataBindingUtil.setContentView(this, R.layout.activity_repository_details);

        viewModel = new RepositoryDetailsActivityViewModel(this);
        binding.setViewModel(viewModel);

        viewModel.setRepository(repository);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    public static Intent createOpenRepositoryDetailsIntent(Context context, Repository repository) {
        Intent intent = new Intent(context, RepositoryDetailsActivity.class);
        intent.putExtra(INTENT_REPOSITORY, repository);
        return intent;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void openLinkInBrowser(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));

        startActivity(intent);
    }
}
