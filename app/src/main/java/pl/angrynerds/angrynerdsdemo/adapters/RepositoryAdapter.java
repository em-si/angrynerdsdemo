package pl.angrynerds.angrynerdsdemo.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import pl.angrynerds.angrynerdsdemo.R;
import pl.angrynerds.angrynerdsdemo.adapters.interfaces.IRepositoryClickListener;
import pl.angrynerds.angrynerdsdemo.models.Repository;
import pl.angrynerds.angrynerdsdemo.databinding.ViewRepositoryBinding;
import pl.angrynerds.angrynerdsdemo.viewmodels.RepositoryListItemViewModel;

/**
 * Created by emsi on 01.05.2017.
 */

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.ViewHolder> {

    private List<Repository> repositories;
    private IRepositoryClickListener repositoryClickListener;
    private Context context;

    public RepositoryAdapter(IRepositoryClickListener repositoryClickListener, Context context) {
        this.repositoryClickListener = repositoryClickListener;
        this.context = context;
    }

    public void setRepositories(List<Repository> repositories) {
        this.repositories = repositories;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final ViewRepositoryBinding binding;

        public ViewHolder(View view) {
            super(view);
            this.binding = DataBindingUtil.bind(view);
        }

        public ViewRepositoryBinding getBinding() {
            return binding;
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_repository, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        RepositoryListItemViewModel viewModel = new RepositoryListItemViewModel(repositoryClickListener);
        viewModel.setRepository(repositories.get(position));

        holder.getBinding().setViewModel(viewModel);
    }

    @Override
    public int getItemCount() {
        return repositories != null ? repositories.size() : 0;
    }
}
