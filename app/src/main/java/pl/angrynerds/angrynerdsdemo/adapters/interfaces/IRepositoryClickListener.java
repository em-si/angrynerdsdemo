package pl.angrynerds.angrynerdsdemo.adapters.interfaces;

import pl.angrynerds.angrynerdsdemo.models.Repository;

/**
 * Created by emsi on 03.05.2017.
 */

public interface IRepositoryClickListener {

    void repositoryClicked(Repository repository);

}
