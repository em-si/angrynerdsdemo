package pl.angrynerds.angrynerdsdemo.apiconnection;

import java.util.List;

import pl.angrynerds.angrynerdsdemo.models.Repository;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by emsi on 03.05.2017.
 */

public interface IApiEndpoint {

    @GET("users/google/repos")
    Call<List<Repository>> getGoogleRepositories();

}
