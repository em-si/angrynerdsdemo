package pl.angrynerds.angrynerdsdemo.apiconnection;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by emsi on 03.05.2017.
 */

public final class RestClient {

    private static final String BASE_URL = "https://api.github.com/";
    private static IApiEndpoint restService = null;

    public static IApiEndpoint getClient() {
        if (restService == null) {

            final Retrofit retrofit = new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(BASE_URL)
                    .build();

            restService = retrofit.create(IApiEndpoint.class);
        }

        return restService;
    }

    public static void clear() {
        restService = null;
    }
}
