package pl.angrynerds.angrynerdsdemo.viewmodels;

import android.databinding.BaseObservable;
import android.support.annotation.NonNull;

import pl.angrynerds.angrynerdsdemo.adapters.interfaces.IRepositoryClickListener;
import pl.angrynerds.angrynerdsdemo.models.Repository;
import pl.angrynerds.angrynerdsdemo.viewmodels.interfaces.IMainActivityAccess;

/**
 * Created by emsi on 03.05.2017.
 */

public class MainActivityViewModel extends BaseObservable implements IRepositoryClickListener {

    private IMainActivityAccess access;

    public MainActivityViewModel(@NonNull IMainActivityAccess access) {
        this.access = access;
    }

    @Override
    public void repositoryClicked(Repository repository) {
        access.openRepository(repository);
    }
}
