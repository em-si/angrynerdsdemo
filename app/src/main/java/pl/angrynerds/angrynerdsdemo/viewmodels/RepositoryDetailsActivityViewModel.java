package pl.angrynerds.angrynerdsdemo.viewmodels;

import android.databinding.BaseObservable;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;
import android.view.View;

import pl.angrynerds.angrynerdsdemo.adapters.interfaces.IRepositoryClickListener;
import pl.angrynerds.angrynerdsdemo.models.Repository;
import pl.angrynerds.angrynerdsdemo.viewmodels.interfaces.IRepositoryDetailsActivityAccess;

/**
 * Created by emsi on 03.05.2017.
 */

public class RepositoryDetailsActivityViewModel extends BaseObservable {

    private IRepositoryDetailsActivityAccess access;
    private ObservableField<Repository> repository;

    public RepositoryDetailsActivityViewModel(@NonNull IRepositoryDetailsActivityAccess access) {
        this.access = access;
        repository = new ObservableField<>();
    }

    public Repository getRepository() {
        return repository.get();
    }

    public void setRepository(Repository repository) {
        this.repository.set(repository);
    }

    public void openLinkInBrowser(View v) {
        access.openLinkInBrowser(repository.get().getHtmlUrl());
    }
}
