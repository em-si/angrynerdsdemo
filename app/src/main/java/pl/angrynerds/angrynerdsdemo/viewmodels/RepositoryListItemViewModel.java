package pl.angrynerds.angrynerdsdemo.viewmodels;

import android.databinding.BaseObservable;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;
import android.view.View;

import pl.angrynerds.angrynerdsdemo.adapters.interfaces.IRepositoryClickListener;
import pl.angrynerds.angrynerdsdemo.models.Repository;

/**
 * Created by emsi on 03.05.2017.
 */

public class RepositoryListItemViewModel extends BaseObservable {

    private IRepositoryClickListener repositoryClickListener;
    private ObservableField<Repository> repository;

    public RepositoryListItemViewModel(@NonNull IRepositoryClickListener repositoryClickListener) {
        this.repositoryClickListener = repositoryClickListener;
        repository = new ObservableField<>();
    }

    public Repository getRepository() {
        return repository.get();
    }

    public void setRepository(Repository repository) {
        this.repository.set(repository);
    }

    public void repositoryClick(View v) {
        repositoryClickListener.repositoryClicked(repository.get());
    }

}
