package pl.angrynerds.angrynerdsdemo.viewmodels.interfaces;

import pl.angrynerds.angrynerdsdemo.models.Repository;

/**
 * Created by emsi on 03.05.2017.
 */

public interface IMainActivityAccess {

    void openRepository(Repository repository);

}
