package pl.angrynerds.angrynerdsdemo.viewmodels.interfaces;

/**
 * Created by emsi on 04.05.2017.
 */

public interface IRepositoryDetailsActivityAccess {

    void openLinkInBrowser(String url);

}
